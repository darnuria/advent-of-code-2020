fn main() -> Result<(), Box<dyn std::error::Error + 'static>> {
    let input = std::env::args().nth(1).ok_or("Wroooong")?;
    let tree_hit = std::fs::read_to_string(input)?
        .lines()
        .skip(1)
        .enumerate()
        .map(|(index, e)| {
            // cynlinder world thing
            let index = ((index + 1) * 3) % e.len();
            (e.as_bytes()[index as usize] == '#' as u8) as u32
        })
        .sum::<u32>();
    println!("{}", tree_hit);
    Ok(())
}
