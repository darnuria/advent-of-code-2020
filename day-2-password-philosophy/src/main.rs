#[derive(Debug)]
struct RangeParseError;
impl std::fmt::Display for RangeParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "RangeParseError")
    }
}
impl std::error::Error for RangeParseError {}

fn main() -> Result<(), Box<dyn std::error::Error + 'static>> {
    let files = std::fs::read_to_string("input")?;
    let part1 = |(min, max, c, password): (u32, u32, u8, &str)| {
        let occurences = password
            .as_bytes()
            .iter()
            .filter(|&&e| e == c)
            .count() as u32;
        (min..=max).contains(&occurences) as u32
    };

    let sum: u32 = files
        .lines()
        .filter_map(|l| {
            let mut i = l.split_whitespace();
            if let (Some(range), Some(c), Some(password)) = (i.next(), i.next(), i.next()) {
                let mut range = range.split('-').filter_map(|e| e.parse::<u32>().ok());
                let min = range.next().expect("no min!");
                let max = range.next().expect("no max!");
                let c = c.as_bytes()[0];
                return Some((min, max, c, password));
            }
            None
        })
        .map(|(must, not, c, password)| {
            // part2
            // Not that so idiomatic but was 23:39 and wanted a quick solution
            // So I let the C programmer express itself.
            // Please fill an issue if this code is too wrong.
            // Would be glad to know.
            let password = password.as_bytes();
            let a = password[(must - 1) as usize] == c;
            let b = password[(not - 1) as usize] == c;
            (a ^ b) as u32
        })
        .sum();

    println!("{}", sum);
    Ok(())
}
