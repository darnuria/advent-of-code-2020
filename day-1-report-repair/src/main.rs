fn main() -> Result<(), Box<dyn std::error::Error + 'static>> {
    let input = std::env::args().nth(1).ok_or("Nay not enough arrrggggl")?;
    let numbers = std::fs::read_to_string(&input)?
        .lines()
        .filter_map(|l| l.parse::<i32>().ok())
        .collect::<std::collections::HashSet<i32>>();

    let n = numbers
        .iter()
        .find_map(|e| {
            numbers.iter().find_map(|i| {
                let rem = 2020i32 - e - i;
                numbers.get(&rem).map(|n| e * n * i)
            })
        })
        .ok_or("No result ;'(")?;
    println!("{}", n);
    Ok(())
}
