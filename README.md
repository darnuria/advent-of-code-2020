# Mes solutions du calendrier de Noël du code 2020 en Rust

[Advent of code 2020](https://adventofcode.com/2020/)

- [x] [jour 1](https://adventofcode.com/2020/day/1)
    - [x] part 1
    - [x] part 2

## Compilation - Execution

Aller dans un des jours du calendrier faire : `cargo run --release input`

## Licence 

AGPLv3 pourquoi? Parceque.